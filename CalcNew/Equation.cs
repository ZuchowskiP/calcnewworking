﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcNew
{
    class Equation
    {
        private double _result = 0;
        private string _operand = "";
        public Equation(string p_operand, string p_inputValue)
        {
            _operand = p_operand;
            _result = Convert.ToDouble(p_inputValue);
        }
        public void Arithmetic(out string p_outputLabel, out string p_outputTextBox, double p_SecondInput)
        {
            p_outputLabel = "";
            p_outputLabel += Convert.ToString(_result + _operand + p_SecondInput);
            switch (_operand)
            {
                case ("+"):
                    _result = (_result + p_SecondInput);
                    p_outputTextBox = Convert.ToString(_result);
                    p_outputLabel += " = " + p_outputTextBox;
                    break;
                case ("-"):
                    _result = (_result - p_SecondInput);
                    p_outputTextBox = Convert.ToString(_result);
                    p_outputLabel += " = " + p_outputTextBox;
                    break;
                case ("*"):
                    _result = (_result * p_SecondInput);
                    p_outputTextBox = Convert.ToString(_result);
                    p_outputLabel += " = " + p_outputTextBox;
                    break;
                case ("/"):
                    _result = (_result / p_SecondInput);
                    p_outputTextBox = Convert.ToString(_result);
                    p_outputLabel += " = " + p_outputTextBox;
                    break;
                case ("x^y"):
                    p_outputLabel = Convert.ToString(_result + "^" + p_SecondInput);
                    _result = Math.Pow(_result, p_SecondInput);
                    p_outputTextBox = Convert.ToString(_result);
                    p_outputLabel += " = " + p_outputTextBox;
                    break;
                case ("mod"):
                    _result = (_result % p_SecondInput);
                    p_outputTextBox = Convert.ToString(_result);
                    p_outputLabel += " = " + p_outputTextBox;
                    break;
                case ("x√y"):
                    p_outputLabel = Convert.ToString(_result + "√" + p_SecondInput);
                    _result = Math.Pow(p_SecondInput, 1 / _result);
                    p_outputTextBox = Convert.ToString(_result);
                    p_outputLabel += " = " + p_outputTextBox;
                    break;
                default:
                    p_outputTextBox = "ERROR";
                    break;
            }
        }
    }
}
