﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcNew
{
    class SingleEntityOp
    {
        private double _result = 0;
        private string _operand = "";
        public SingleEntityOp(string p_operand, double p_inputValue)
        {
            _operand = p_operand;
            _result = p_inputValue;

        }
        public void Operation(out string p_outputLabel, out string p_outputTextBox)
        {
            p_outputLabel = "";
            p_outputTextBox = "";
            switch (_operand)
            {
                case ("√y"):
                    p_outputLabel = Convert.ToString("√" + _result);
                    _result = Math.Sqrt(_result);
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("π"):
                    p_outputTextBox = Convert.ToString(Math.PI);
                    break;
                case ("e"):
                    p_outputTextBox = Convert.ToString(Math.E);
                    break;
                case ("1/x"):
                    p_outputLabel = Convert.ToString("1/" + _result);
                    _result = 1.0 / Convert.ToDouble(_result);
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("|x|"):
                    p_outputLabel = Convert.ToString("|" + _result + "|");
                    if (_result < 0)
                    {
                        _result = -_result;
                    }
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("exp"):
                    p_outputLabel = Convert.ToString("exp" + _result);
                    _result = Math.Exp(_result);
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("x^2"):
                    p_outputLabel = Convert.ToString(_result + "^2");
                    _result = _result * _result;
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("10^x"):
                    p_outputLabel = Convert.ToString("10^" + _result);
                    _result = Math.Pow(10, _result);
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("log"):
                    p_outputLabel = Convert.ToString("log" + _result);
                    _result = Math.Log(_result);
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("n!"):
                    double f = 1;
                    double num = _result;
                    p_outputLabel = Convert.ToString(_result + "!");
                    if (num == 0)
                    {
                        num = 1;
                        p_outputTextBox = Convert.ToString(num);
                    }
                    else
                    {
                        for (int i = 1; i <= num; i++)
                        {
                            f = f * i;
                            p_outputTextBox = Convert.ToString(f);
                        }
                    }
                    break;
                case ("ln"):
                    p_outputLabel = Convert.ToString("ln" + _result);
                    _result = Math.Log10(_result);
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                case ("+/-"):
                    _result = -_result;
                    p_outputTextBox = Convert.ToString(_result);
                    break;
                default:
                    p_outputTextBox = "ERROR";
                    break;
            }
        }
    }
}
