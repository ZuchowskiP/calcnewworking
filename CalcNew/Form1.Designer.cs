﻿namespace CalcNew
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRes = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.trygonometryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sinhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tanhToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblRes
            // 
            this.lblRes.AutoSize = true;
            this.lblRes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.8F);
            this.lblRes.Location = new System.Drawing.Point(135, 27);
            this.lblRes.Name = "lblRes";
            this.lblRes.Size = new System.Drawing.Size(0, 18);
            this.lblRes.TabIndex = 80;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.75F);
            this.richTextBox1.Location = new System.Drawing.Point(15, 48);
            this.richTextBox1.MaxLength = 17;
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(624, 82);
            this.richTextBox1.TabIndex = 79;
            this.richTextBox1.Text = "0";
            // 
            // button31
            // 
            this.button31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button31.Location = new System.Drawing.Point(519, 582);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(120, 64);
            this.button31.TabIndex = 78;
            this.button31.Text = "=";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.Eq);
            // 
            // button32
            // 
            this.button32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button32.Location = new System.Drawing.Point(393, 582);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(120, 64);
            this.button32.TabIndex = 77;
            this.button32.Text = ".";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.Numbers);
            // 
            // button33
            // 
            this.button33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button33.Location = new System.Drawing.Point(267, 582);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(120, 64);
            this.button33.TabIndex = 76;
            this.button33.Text = "0";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.Numbers);
            // 
            // button34
            // 
            this.button34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button34.Location = new System.Drawing.Point(141, 582);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(120, 64);
            this.button34.TabIndex = 75;
            this.button34.Text = "+/-";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button35
            // 
            this.button35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button35.Location = new System.Drawing.Point(15, 582);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(120, 64);
            this.button35.TabIndex = 74;
            this.button35.Text = "ln";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button26
            // 
            this.button26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button26.Location = new System.Drawing.Point(519, 512);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(120, 64);
            this.button26.TabIndex = 73;
            this.button26.Text = "+";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.Operations);
            // 
            // button27
            // 
            this.button27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button27.Location = new System.Drawing.Point(393, 512);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(120, 64);
            this.button27.TabIndex = 72;
            this.button27.Text = "3";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.Numbers);
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button28.Location = new System.Drawing.Point(267, 512);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(120, 64);
            this.button28.TabIndex = 71;
            this.button28.Text = "2";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.Numbers);
            // 
            // button29
            // 
            this.button29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button29.Location = new System.Drawing.Point(141, 512);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(120, 64);
            this.button29.TabIndex = 70;
            this.button29.Text = "1";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.Numbers);
            // 
            // button30
            // 
            this.button30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button30.Location = new System.Drawing.Point(15, 512);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(120, 64);
            this.button30.TabIndex = 69;
            this.button30.Text = "log";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button21
            // 
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button21.Location = new System.Drawing.Point(519, 442);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(120, 64);
            this.button21.TabIndex = 68;
            this.button21.Text = "-";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.Operations);
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button22.Location = new System.Drawing.Point(393, 442);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(120, 64);
            this.button22.TabIndex = 67;
            this.button22.Text = "6";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.Numbers);
            // 
            // button23
            // 
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button23.Location = new System.Drawing.Point(267, 442);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(120, 64);
            this.button23.TabIndex = 66;
            this.button23.Text = "5";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.Numbers);
            // 
            // button24
            // 
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button24.Location = new System.Drawing.Point(141, 442);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(120, 64);
            this.button24.TabIndex = 65;
            this.button24.Text = "4";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.Numbers);
            // 
            // button25
            // 
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button25.Location = new System.Drawing.Point(15, 442);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(120, 64);
            this.button25.TabIndex = 64;
            this.button25.Text = "10^x";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button16.Location = new System.Drawing.Point(519, 372);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(120, 64);
            this.button16.TabIndex = 63;
            this.button16.Text = "*";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.Operations);
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button17.Location = new System.Drawing.Point(393, 372);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(120, 64);
            this.button17.TabIndex = 62;
            this.button17.Text = "9";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.Numbers);
            // 
            // button18
            // 
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button18.Location = new System.Drawing.Point(267, 372);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(120, 64);
            this.button18.TabIndex = 61;
            this.button18.Text = "8";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.Numbers);
            // 
            // button19
            // 
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button19.Location = new System.Drawing.Point(141, 372);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(120, 64);
            this.button19.TabIndex = 60;
            this.button19.Text = "7";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.Numbers);
            // 
            // button20
            // 
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button20.Location = new System.Drawing.Point(15, 372);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(120, 64);
            this.button20.TabIndex = 59;
            this.button20.Text = "x^2";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button11.Location = new System.Drawing.Point(519, 302);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(120, 64);
            this.button11.TabIndex = 58;
            this.button11.Text = "/";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.Operations);
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button12.Location = new System.Drawing.Point(393, 302);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(120, 64);
            this.button12.TabIndex = 57;
            this.button12.Text = "n!";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button15.Location = new System.Drawing.Point(15, 302);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(120, 64);
            this.button15.TabIndex = 54;
            this.button15.Text = "x^y";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.Operations);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button6.Location = new System.Drawing.Point(267, 162);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(120, 64);
            this.button6.TabIndex = 53;
            this.button6.Text = "mod";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Operations);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button7.Location = new System.Drawing.Point(141, 162);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(120, 64);
            this.button7.TabIndex = 52;
            this.button7.Text = "exp";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button8.Location = new System.Drawing.Point(267, 302);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(120, 64);
            this.button8.TabIndex = 51;
            this.button8.Text = "|x|";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // cos
            // 
            this.cos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.cos.Location = new System.Drawing.Point(141, 302);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(120, 64);
            this.cos.TabIndex = 50;
            this.cos.Text = "1/x";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button10.Location = new System.Drawing.Point(15, 232);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(120, 64);
            this.button10.TabIndex = 49;
            this.button10.Text = "√y";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.Highlight;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.Location = new System.Drawing.Point(393, 162);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(246, 64);
            this.button5.TabIndex = 48;
            this.button5.Text = "Backspace";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.Backspace);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.Highlight;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button4.Location = new System.Drawing.Point(393, 232);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(246, 64);
            this.button4.TabIndex = 47;
            this.button4.Text = "CE";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.CE);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button3.Location = new System.Drawing.Point(267, 232);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 64);
            this.button3.TabIndex = 46;
            this.button3.Text = "e";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button2.Location = new System.Drawing.Point(141, 232);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(120, 64);
            this.button2.TabIndex = 45;
            this.button2.Text = "π";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SingleEntityOperations);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.button1.Location = new System.Drawing.Point(15, 162);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 64);
            this.button1.TabIndex = 44;
            this.button1.Text = "x√y";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Operations);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trygonometryToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(654, 28);
            this.menuStrip1.TabIndex = 81;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // trygonometryToolStripMenuItem
            // 
            this.trygonometryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sinToolStripMenuItem,
            this.sinhToolStripMenuItem,
            this.cosToolStripMenuItem,
            this.coshToolStripMenuItem,
            this.tanToolStripMenuItem,
            this.tanhToolStripMenuItem});
            this.trygonometryToolStripMenuItem.Name = "trygonometryToolStripMenuItem";
            this.trygonometryToolStripMenuItem.Size = new System.Drawing.Size(115, 24);
            this.trygonometryToolStripMenuItem.Text = "Trygonometry";
            // 
            // sinToolStripMenuItem
            // 
            this.sinToolStripMenuItem.Name = "sinToolStripMenuItem";
            this.sinToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.sinToolStripMenuItem.Text = "Sin";
            this.sinToolStripMenuItem.Click += new System.EventHandler(this.sinToolStripMenuItem_Click);
            // 
            // sinhToolStripMenuItem
            // 
            this.sinhToolStripMenuItem.Name = "sinhToolStripMenuItem";
            this.sinhToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.sinhToolStripMenuItem.Text = "Sinh";
            this.sinhToolStripMenuItem.Click += new System.EventHandler(this.sinhToolStripMenuItem_Click);
            // 
            // cosToolStripMenuItem
            // 
            this.cosToolStripMenuItem.Name = "cosToolStripMenuItem";
            this.cosToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.cosToolStripMenuItem.Text = "Cos";
            this.cosToolStripMenuItem.Click += new System.EventHandler(this.cosToolStripMenuItem_Click);
            // 
            // coshToolStripMenuItem
            // 
            this.coshToolStripMenuItem.Name = "coshToolStripMenuItem";
            this.coshToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.coshToolStripMenuItem.Text = "Cosh";
            this.coshToolStripMenuItem.Click += new System.EventHandler(this.coshToolStripMenuItem_Click);
            // 
            // tanToolStripMenuItem
            // 
            this.tanToolStripMenuItem.Name = "tanToolStripMenuItem";
            this.tanToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.tanToolStripMenuItem.Text = "Tan";
            this.tanToolStripMenuItem.Click += new System.EventHandler(this.tanToolStripMenuItem_Click);
            // 
            // tanhToolStripMenuItem
            // 
            this.tanhToolStripMenuItem.Name = "tanhToolStripMenuItem";
            this.tanhToolStripMenuItem.Size = new System.Drawing.Size(124, 26);
            this.tanhToolStripMenuItem.Text = "Tanh";
            this.tanhToolStripMenuItem.Click += new System.EventHandler(this.tanhToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 662);
            this.Controls.Add(this.lblRes);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Calculator";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRes;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem trygonometryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sinhToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tanhToolStripMenuItem;
    }
}

