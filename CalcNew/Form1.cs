﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalcNew
{
    public partial class Form1 : Form
    {
        string operand = "";
        double results = 0;
        string textLabel, textRich;
        public Form1()
        {
            InitializeComponent();
        }
        private void Numbers(object sender, EventArgs e)
        {
            try
            {
                if (richTextBox1.Text == "0")
                {
                    richTextBox1.Text = "";
                }
                Button num = (Button)sender;
                if (richTextBox1.Text == "" && num.Text == ".")
                    richTextBox1.Text = "0.";
                if (num.Text == ".")
                {
                    if (!richTextBox1.Text.Contains("."))
                        richTextBox1.Text += num.Text;

                }
                else
                    richTextBox1.Text += num.Text;
            }
            catch
            {
                richTextBox1.Text = "Incorrect input";
            }
        }
        private void Operations(object sender, EventArgs e)
        {
            try
            {
                Button number = (Button)sender;
                operand = number.Text;
                if (operand == "x√y")
                {
                    results = Convert.ToDouble(richTextBox1.Text);
                    lblRes.Text = richTextBox1.Text + "√";
                    richTextBox1.Text = "";
                }
                else if (operand == "x^y")
                {
                    results = Convert.ToDouble(richTextBox1.Text);
                    lblRes.Text = richTextBox1.Text + "^";
                    richTextBox1.Text = "";
                }
                else
                {
                    results = Convert.ToDouble(richTextBox1.Text);
                    richTextBox1.Text = "";
                    lblRes.Text = Convert.ToString(results) + operand;
                }
            }
            catch
            {
                richTextBox1.Text = "Incorrect input";
            }
        }
        private void CE(object sender, EventArgs e)
        {
            richTextBox1.Text = "0";
            lblRes.Text = "";
        }
        private void Backspace(object sender, EventArgs e)
        {
            if (richTextBox1.Text.Length > 1)
            {
                richTextBox1.Text = richTextBox1.Text.Remove(richTextBox1.Text.Length - 1, 1);
            }
            else
            {
                richTextBox1.Text = "0";
            }
        }
        private void Eq(object sender, EventArgs e)
        {
            Equation crackBabies = new Equation(operand, Convert.ToString(results));
            try
            {
                crackBabies.Arithmetic(out textLabel, out textRich, Convert.ToDouble(richTextBox1.Text));
                richTextBox1.Text = textRich;
                lblRes.Text = textLabel;
            }
            catch
            {
                richTextBox1.Text = "Incorrect input";
            }
        }

        private void sinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                results = Math.Sin(Convert.ToDouble(richTextBox1.Text));
                richTextBox1.Text = Convert.ToString(results);
            }
            catch
            {
                richTextBox1.Text = "ERROR mowilem!";
            }
        }
        private void sinhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                results = Math.Sinh(Convert.ToDouble(richTextBox1.Text));
                richTextBox1.Text = Convert.ToString(results);
            }
            catch
            {
                richTextBox1.Text = "ERROR mowilem!";
            }

        }

        private void cosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                results = Math.Cos(Convert.ToDouble(richTextBox1.Text));
                richTextBox1.Text = Convert.ToString(results);
            }
            catch
            {
                richTextBox1.Text = "ERROR mowilem!";
            }
        }

        private void coshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                results = Math.Cosh(Convert.ToDouble(richTextBox1.Text));
                richTextBox1.Text = Convert.ToString(results);
            }
            catch
            {
                richTextBox1.Text = "ERROR mowilem!";
            }
        }

        private void tanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                results = Math.Tan(Convert.ToDouble(richTextBox1.Text));
                richTextBox1.Text = Convert.ToString(results);
            }
            catch
            {
                richTextBox1.Text = "ERROR mowilem!";
            }
        }

        private void tanhToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                results = Math.Tanh(Convert.ToDouble(richTextBox1.Text));
                richTextBox1.Text = Convert.ToString(results);
            }
            catch
            {
                richTextBox1.Text = "ERROR mowilem!";
            }
        }
        private void SingleEntityOperations(object sender, EventArgs e)
        {
            try
            {
                Button number = (Button)sender;
                operand = number.Text;
                results = Convert.ToDouble(richTextBox1.Text);
                SingleEntityOp beetleJuice = new SingleEntityOp(operand, results);
                beetleJuice.Operation(out textLabel, out textRich);
                richTextBox1.Text = textRich;
                lblRes.Text = textLabel;
            }
            catch
            {
                richTextBox1.Text = "ERROR mowilem!";
            }
        }
    }
}
